---
title: "/線代/ Sec3.10-12"
date: 2004-01-09T01:14:27+08:00
author: seamonster@ofo.twbbs.org
description: "A Primer on Linear Algebra Sec3.10-12"
---

{{< quiz >}}

If $T$ is unitary linear transformation of $\mathcal{F}^n$, and $T(v_1), \cdots, T(v_n)$ is an orthonomoral basis of $\mathcal{F}^n$, show that $v_1, \cdots, v_n$ is an orthonomoral basis of $\mathcal{F}^n$.
{{< /quiz >}}

----

For all $i\neq j$ we have 

$$
\langle v_i, v_j \rangle = \langle v_i, \mathbf{I}(v_j)\rangle = \langle v_i, T^{*}T(v_j)\rangle = \langle T(v_i), T(v_j)\rangle = 0
$$
                  

then it's orthonomoral.

If $a_{1}v_{1} + a_{2}v_{2} + \cdots a_{n}v_{n} = 0$, then 


$$
\begin{aligned}
0=&T(a_{1}v_{1} + a_{2}v_{2} + \cdots + a_{n}v_{n})\newline
=&a_{1}T(v_{1}) + a_{2}T(v_{2}) + \cdots + a_{n}T(v_{n}).
\end{aligned}
$$


Since $T(v_1), \cdots, T(v_n)$ is an orthonomoral basis then $a_i = 0$ for all $i$, then it's basis.
   