---
title: "/線代/ Sec4.4-15"
date: 2004-02-02T00:12:22+08:00
author: seamonster@ofo.twbbs.org
description: "A Primer on Linear Algebra Sec4.4-15"
---

{{< quiz >}}
If $\mathbf{E^2} = \mathbf{E}$ , show that $\operatorname{tr}(\mathbf{E})$ is an interger.
{{< /quiz >}}

----

Since eigenvalue of $\mathbf{E}$ is $1$ or $0$ and $\operatorname {tr}(E)$ is sum of eigenvalue of $\mathbf{E}$, then we are done.

{{< alert type="tip" >}}
這題目就考這個觀念 : **trace = 特徵根和**
{{< /alert >}}