---
cascade:
  tags: ["Linear Algebra"]
---

A Primer on Linear Algebra by [I. N. Herstein](https://en.wikipedia.org/wiki/Israel_Nathan_Herstein), [David J. Winter](https://lsa.umich.edu/math/people/emeritus-faculty/windj.html)