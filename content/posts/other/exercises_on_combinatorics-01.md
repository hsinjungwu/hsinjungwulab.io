---
title: "/組合/ 作業 1018-Q1"
date: 2004-10-11T22:50:03+08:00
author: seamonster@ofo.twbbs.org
description: "組合學 1018 作業 1"
tags: ["Combinatorics"]
---

{{< quiz >}}
Prove that $\lfloor\sqrt{\lfloor{x}\rfloor}\rfloor = \lfloor\sqrt{x}\rfloor$ for any real number $x$.
{{< /quiz >}}

----

It's clearly that there exist a positive integer n, s.t.

$$
\begin{aligned}
& n^2\leq\lfloor{x}\rfloor\leq x < {(n+1)}^2\\
\Rightarrow & n\leq \sqrt{\lfloor{x}\rfloor} \leq \sqrt{x} < n+1\\
\Rightarrow & n\leq \lfloor\sqrt{\lfloor{x}\rfloor}\rfloor \leq \lfloor\sqrt{x}\rfloor < n+1
\end{aligned}
$$

Hence $n = \lfloor\sqrt{\lfloor{x}\rfloor}\rfloor = \lfloor\sqrt{x}\rfloor$