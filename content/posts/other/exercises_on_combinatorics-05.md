---
title: "/組合/ 作業 1018-Q5"
date: 2004-10-12T17:52:50+08:00
author: seamonster@ofo.twbbs.org
description: "組合學 1018 作業 5"
tags: ["Combinatorics"]
---

{{< quiz >}}
Prove that $n$ divides $2n-2 \choose n-1$ where $n\geq1$ is an integer.
{{< /quiz >}}

----

$n=1$ is trivial.

Since ${2n-2 \choose n-1} = \frac{n}{n-1}\times{2n-2 \choose n-2}$ is an integer, where $n>1$. 

We know that $n-1$ and $n$ are relatively prime, so $(n-1)\big\vert {2n-2 \choose n-2}$, that means

$$
\frac{ {2n-2\choose n-1} }{n} = \frac{ {2n-2 \choose n-2} }{n-1} \in \mathbb{Z}
$$

,hence $n\big\vert {2n-2 \choose n-1}$.