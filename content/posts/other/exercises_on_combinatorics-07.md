---
title: "/組合/ 作業 1018-Q7"
date: 2004-10-12T02:36:42+08:00
author: seamonster@ofo.twbbs.org
description: "組合學 1018 作業 7"
tags: ["Combinatorics"]
---

{{< quiz >}}
Let $n$ and $k$ be integers such that $n\geq 2$ and $1\leq k\leq n$. Define 

$$
d_k(n)=\bigg\lvert{n\choose k}-{n\choose k-1}\bigg\rvert
$$

and 

$$
d_{\min}(n)=\min\{d_k(n)\mid 1\leq k\leq n\}.
$$ 

Prove that 

(a) $d_{\min}(n)=0$ if and only if $n$ is odd.


(b)For odd n, $d_k(n) = 0$ if and only if $k = (n+1)/2$


(c) For $n$ different from $4$ and $6$, $d_k(n) = n-1$ if and only if $k=1$ or $k=n$.
{{< /quiz >}}

----

- [proof of (a)](./exercises_on_combinatorics-07-a.md)
- [proof of (b)](./exercises_on_combinatorics-07-b.md)
- [proof of (c)](./exercises_on_combinatorics-07-c.md)