---
title: "/組合/ 作業 1018-Q7-b"
date: 2004-10-12T02:36:42+08:00
author: seamonster@ofo.twbbs.org
description: "組合學 1018 作業 7 (b) 解答"
tags: ["Combinatorics"]
---

The necessary part is proved by [a](./exercises_on_combinatorics-07-a.md)'s necessary part.

By [a](./exercises_on_combinatorics-07-a.md)'s sufficient part, we have $k = n-k+1$, then $k = \frac{n+1}{2}.$ So the sufficient part is proved.