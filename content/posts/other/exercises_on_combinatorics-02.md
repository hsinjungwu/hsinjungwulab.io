---
title: "/組合/ 作業 1018-Q2"
date: 2004-10-11T23:22:23+08:00
author: seamonster@ofo.twbbs.org
description: "組合學 1018 作業 2"
tags: ["Combinatorics"]
---

{{< quiz >}}
Prove for positive integers $m$ and $n$ that

$$
n =\lceil{\frac{n}{m}}\rceil+\lceil{\frac{n-1}{m}}\rceil+\ldots+\lceil{\frac{n-m+1}{m}}\rceil
$$
{{< /quiz >}}

----

Let $n = km+r$, where $k$ is a nonegative integer and $0\leq r < m$. 

Since $0 \leq r < m$, we have

$$
\lceil\frac{r-i}{m}\rceil = 
\begin{cases}
1, &\text{where } 0\leq i\leq r-1\\
0, &\text{where } r\leq i\leq m-1
\end{cases}
$$

for nonegative integer $i$.

Then we have 

$$
\begin{aligned}
\sum\limits_{i=0}^{m-1}\lceil\frac{n}{m}\rceil&=\sum\limits_{i=0}^{m-1}\lceil\frac{km+r-i}{m}\rceil\\
&=\sum\limits_{i=0}^{m-1}\big(k+\lceil\frac{r-i}{m}\rceil\big)\\
&=km+\sum\limits_{i=0}^{m-1}\lceil\frac{r-i}{m}\rceil\\
&=km+\sum\limits_{i=0}^{r-1}\lceil{\frac{r-i}{m}}\rceil+\sum\limits_{i=r}^{m-1}\lceil{\frac{r-i}{m}}\rceil\\
&=km+\overbrace{1+\ldots+1}^{r}+\overbrace{0+\ldots+0}^{m-r}\\
&=km+r=n
\end{aligned}
$$