---
title: "/組合/ 作業 1018-Q7-a"
date: 2004-10-12T02:36:42+08:00
author: seamonster@ofo.twbbs.org
description: "組合學 1018 作業 7 (a) 解答"
tags: ["Combinatorics"]
---

### $\implies$

If $d_{\min}(n) = 0$ then $d_k(n) = 0$ for some $k$. Then

$$
\begin{aligned}
&\quad &{n\choose k} &= {n\choose k-1}\\
&\Rightarrow &(n-k)!k! &= (n-k+1)!(k-1)!\\
&\Rightarrow &k &= (n-k+1)\\
&\Rightarrow &n &= 2k-1
\end{aligned}
$$

, hence $n$ is odd.

### $\impliedby$

If $n$ is odd, pick $k = (n+1)/2$, then 

$$ 
0 \leq d_{\min}(n) \leq d_k(n) = \bigg\lvert {n\choose k}-{n\choose k-1} \bigg\rvert = 0.
$$