---
title: "/高微/ 2001 交大應數碩士班考試 - 高微 Q2"
date: 2004-03-30T23:58:25+08:00
author: seamonster@ofo.twbbs.org
description: "Principles of Mathematical Analysis Ch06-14"
tags: ["Analysis"]
---

{{< quiz >}}
Let $f(x)=\int_x^{x+1}\sin(e^t)dt$. Prove $e^x|f(x)|<2$.
{{< /quiz >}}

----

Let $u=e^t$. Then

$$
\begin{aligned}
f(x) &= \int_{e^x}^{e^{x+1}}\frac{\sin(u)}{u}du\newline
&= \bigg(\frac{\cos(e^x)}{e^x}-\frac{\cos(e^{x+1})}{e^{x+1}}\bigg)-\int_{e^x}^{e^{x+1}}\frac{\cos(u)}{u^2}du\newline
&\leq \bigg(\frac{\cos(e^x)}{e^x}-\frac{\cos(e^{x+1})}{e^{x+1}}\bigg)+\int_{e^x}^{e^{x+1}}\frac{1}{u^2}du\newline
&=\frac{\cos(e^x)}{e^x}-\frac{\cos(e^{x+1})}{e^{x+1}}+\frac{1}{e^x}-\frac{1}{e^{x+1}}
\end{aligned}
$$


So 

$$
\begin{aligned}
e^x|f(x)|&\leq\bigg|\cos(e^x)-\frac{\cos(e^{x+1})}{e}\bigg|+1-\frac{1}{e}\newline
&\leq\bigg|\cos(e^x)\bigg|+\bigg|\frac{\cos(e^{x+1})}{e}\bigg|+1-\frac{1}{e}
\end{aligned}
$$

However, it is impossible that $\cos(e^x)=\cos(e^{x+1})=1$ for some $x$. 

Otherwise we would have $e^x=2n\pi$ and $e^{x+1}=2m\pi$, which implies that $e$ is a rational number $\frac{m}{n}$. Contradiction !

Hence
$$
e^x|f(x)|<1+\frac{1}{e}+1-\frac{1}{e}=2
$$

{{< alert type="danger" >}}
老實說，我一直覺得交大應數碩班的考古題很機啊！ 😒
{{< /alert >}}