---
title: "/組合/ 作業 1018-Q3"
date: 2004-10-12T14:43:12+08:00
author: seamonster@ofo.twbbs.org
description: "組合學 1018 作業 3"
tags: ["Combinatorics"]
---

{{< quiz >}}
Prove the following [theorem of Legendre](https://en.wikipedia.org/wiki/Legendre%27s_formula). 

If $p$ is a prime and $\mu$ is the highest power of $p$ dividing $n!$, then

$$
\mu=\sum\limits_{i=1}^{\infty}\lfloor\frac{n}{p^i}\rfloor
$$
{{< /quiz >}}

----

Hint : the number of $p^i$-multiple is $\lfloor\frac{n}{p^i}\rfloor$, for integer $i > 0$.