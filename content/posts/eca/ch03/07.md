---
title: "/高微/ Ch03-07"
date: 2004-01-07T20:31:46+08:00
author: seamonster@ofo.twbbs.org
description: "Elementary Classical Analysis (2nd Edition) Ch03-07"
---

{{< quiz >}}
Let $x_k$ be a sequence in $\mathbb{R}^n$ that converges to $x$ and let $A_k = \{ x_k, x_{k+1}, \cdots \}$, show that $\lbrace x\rbrace=\cap_{k=1}^{\infty}\operatorname{cl}(A_k).$ Is this true in any metric space ?
{{< /quiz >}}

----

It's obvious that $x\in\operatorname{acc}(A_k)$ for all $k$, then $\lbrace x\rbrace\subset\cap_{k=1}^{\infty}\operatorname{cl}(A_k)$

Suppose there is $y\neq x$ also in $\cap_{k=1}^{\infty}\operatorname{cl}(A_k)$.

Since $\cap_{k=1}^{\infty}A_k=\emptyset$, $A_k\neq\emptyset$, $A_{k+1}\subset A_k$, by Ch 2-41, $y\in\operatorname{acc}(A_1)$.

Thus $\lbrace x_k\rbrace$ is converge to $y$, then $y = x$, contradiction!

----

Yes, this is true in any metric space. 