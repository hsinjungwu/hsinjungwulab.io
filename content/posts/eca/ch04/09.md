---
title: "/高微/ Ch04-09"
date: 2004-02-18T13:01:00+08:00
author: seamonster@ofo.twbbs.org
description: "Elementary Classical Analysis (2nd Edition) Ch04-09"
---

{{< quiz >}}
Prove the following "gluing lemma": Let $f:[a,b]\to\mathbb{R}^m$ and $g:[b,c]→\mathbb{R}^m$ be continuous. Define $h:[a,c]→\mathbb{R}^m$ by $h = f$ on $[a,b]$ and $h = g$ on $[b,c]$, then $h$ is continuous.
{{< /quiz >}}

----

Since $g(b) = h(b) = f(b) $, for all $\epsilon > 0$, there is an $\delta_1,\delta_2$  s.t. 

$$
\begin{aligned}
&\|x-b\| < \delta_1 , &\|f(x)-f(b)\| < \epsilon\newline
&\|x-b\| < \delta_2 , &\|g(x)-g(b)\| < \epsilon.
\end{aligned}
$$

then we choose $\delta=\min(\delta_1,\delta_2) > 0$ s.t. $\\|x-b\\| < \delta$ then $\\|h(x) - h(b)\\| < \delta$.