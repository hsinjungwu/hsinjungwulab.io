---
title: "/高微/ Ch04-21"
date: 2004-02-18T20:04:27+08:00
author: seamonster@ofo.twbbs.org
description: "Elementary Classical Analysis (2nd Edition) Ch04-21"
---

{{< quiz >}}
Which of the following functions on $\mathbb{R}$ are uniformly continuous？

- (a) $f(x) = 1/(x^2+1)$ 
- (b) $f(x) = \cos^3(x)$
- (c) $f(x) = x^2 / (x^2+2)$
- (d) $f(x) = x\sin(x)$
{{< /quiz >}}

----

### (a) Yes
For all $\epsilon$ > 0, for every $x,y\in\mathbb{R}$, there is a $\delta =\epsilon$ s.t. $\mid x-y\mid < \delta$, implies 

$$
\begin{aligned}
\mid f(x)-f(y)\mid &= \frac{\mid x-y\mid\times\mid x+y\mid}{(x^2+1)(y^2+1)}\\
&<\frac{\mid x-y\mid\times(\mid x\mid+\mid y\mid)}{(x^2+1)(y^2+1)}\\
&=\mid x-y\mid\times(\frac{1}{2(x^2+1)}+ \frac{1}{2(y^2+1)})\\
&\leq\delta = \epsilon.
\end{aligned}
$$

### (b) Yes

$$
\begin{aligned}
\mid f'(x)\mid &= 3\mid\cos^2(x)\sin(x)\mid\\
&\leq 3\mid(1-\sin^2(x))\sin(x)\mid\\
&\leq 3\mid \sin(x)-\sin^3(x)\mid\\
&\leq 3(\mid\sin(x)\mid + \mid\sin^3(x)\mid) < 6
\end{aligned}
$$

then it's unif conti.

### (c) Yes 

like (a), since $f(x) = x^2/(x^2+2) = 1-2/(x^2 +2)$

### (d) No

From continuity of $x\sin(x)$, you have that $\forall\epsilon>0,\forall y,\exist\delta$,

$$
\mid 𝑥−y\mid<\delta\implies\mid x\sin(x)−y\sin(y)\mid<\delta
$$

Define $p=x+2n\pi$, $q=y+2n\pi$

Then you have $\mid p-q\mid<\delta$ but

$$
\begin{aligned}
&\mid (x+2n\pi)\sin(x+2n\pi)−(y+2n\pi)\sin(y+2n\pi)\mid\\
=&\mid(x\sin(x)−y\sin(y))+2n\pi(\sin(x)−\sin(y))\mid>\delta
\end{aligned}
$$

for sufficiently large $n.$ An undesirable result for uniform continuity.

{{< alert >}}
(d) 我當初沒解出來，今天(2025/03/12)參考了這篇 [Uniform Continuity of xsinx](https://math.stackexchange.com/questions/372766/uniform-continuity-of-x-sin-x)
{{< /alert >}}