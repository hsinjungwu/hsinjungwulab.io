---
title: "/高微/ Ch01-07"
date: 2003-12-17T15:39:25+08:00
author: seamonster@ofo.twbbs.org
description: "Elementary Classical Analysis (2nd Edition) Ch01-07"
---

{{< quiz >}}
For nonempty sets $A,B\subset\mathbb{R}$, let $A+B = \lbrace\ x+y \mid x\in A\text{ and }y\in B\ \rbrace.$


Show that $\sup(A+B) = \sup(A)+\sup(B)$
{{< /quiz >}}

----

Since $x\leq\sup(A)$ and $y\leq\sup(B)$, we have $x+y\leq\sup(A)+\sup(B)$ then 

$$
\sup(A+B)\leq\sup(A)+\sup(B)\tag{1}
$$

For all $\epsilon > 0$, $\sup(A)-\epsilon < x$ and $\sup(B)-\epsilon < y$, then

$$
\sup(A)+\sup(B)-2\epsilon < x+y\leq\sup(A+B)
$$

Since $\epsilon$ is arbitrary, then

$$
\sup(A)+\sup(B)\leq\sup(A+B)\tag{2}
$$

By $(1), (2)$, we get the conclusion.