---
cascade:
  tags: ["Analysis"]
---

Elementary Classical Analysis (2nd Edition) by [Jerrold E. Marsden](http://www.cds.caltech.edu/~marsden/), Michael J. Hoffman;
