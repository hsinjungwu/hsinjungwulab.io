---
title: "/代數/ Sec2.3-04"
date: 2003-12-06T20:33:25+08:00
author: seamonster@ofo.twbbs.org
description: "Topics in Algebra Sec2.3-04"
---

{{< quiz >}}
If $G$ is a group in which $(a\cdot b)^i = a^{i} \cdot b^{i}$, for three consecutive integers $i$ for all $a, b$ in $G$, show that $G$ is abelian.
{{< /quiz >}}

----

Since 

$$
a^{i+2}\cdot b^{i+2} = a\cdot a^{i+1}\cdot b^{i+1}\cdot b = a\cdot (ab)^{i+1}\cdot b
$$

and $(ab)^{i+2} = a\cdot (ba)^{i+1}\cdot b$, then $(ab)^{i+1} = (ba)^{i+1}.$

In same way, $(ab)^i = (ba)^i.$ Then 

$$
ab\cdot(ba)^i = ab\cdot(ab)^i = (ab)^{i+1} = (ba)^{i+1} = ba\cdot(ba)^i
$$

hence $a\cdot b = b\cdot a.$

{{< alert >}}
$a, b, (ba)^{i}$ has inverse, beacause they are in $G$. 
{{< /alert >}}