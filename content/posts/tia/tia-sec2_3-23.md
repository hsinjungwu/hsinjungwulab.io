---
title: "/代數/ Sec2.3-23"
date: 2003-12-06T23:25:36+08:00
author: seamonster@ofo.twbbs.org
description: "Topics in Algebra Sec2.3-23"
---

{{< quiz >}}
Construct in the $G$ (which is the set of all real matrices $\begin{bmatrix}a&b\newline 0&d\end{bmatrix}$ , where $ad\neq 0.$) a subgroup of order 4.
{{< /quiz >}}

----

$$
S = \left\lbrace
\begin{bmatrix} 1 & 0 \newline 0 & 1 \end{bmatrix},\quad
\begin{bmatrix} 1 & 0 \newline 0 & -1 \end{bmatrix},\quad
\begin{bmatrix} -1 & 0 \newline 0 & -1 \end{bmatrix},\quad
\begin{bmatrix} -1 & 0 \newline 0 & 1 \end{bmatrix}
\right\rbrace
$$
