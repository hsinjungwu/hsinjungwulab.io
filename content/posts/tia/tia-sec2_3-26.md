---
title: "/代數/ Sec2.3-26"
date: 2003-12-07T01:12:52+08:00
author: seamonster@ofo.twbbs.org
description: "Topics in Algebra Sec2.3-26"
---

{{< quiz >}}
(a) Let $G$ be the group of all $2\times 2$ matrices $\begin{bmatrix}a&b\newline c&d\end{bmatrix}$ where $a,b,c,d$ are intergers modulo $p$, $p$ a prime number, such that $ad-bc\neq0$. $G$ forms a group relative to matrix multiplication. What is $o(G)$ ?


(b) Let $H$ be the subgroup of $G$ of part(a) defined by

$$
H = \bigg\lbrace \begin{bmatrix}a&b\newline c&d\end{bmatrix}\text{ in } G\mid ad-bc = 1\bigg\rbrace.
$$

What is $o(H)$?
{{< /quiz >}}

----

### (a)

Let $\lbrace\ [0],[1],\cdots, [p-1]\ \rbrace$ be the set of the congruence classes mod $p$. 

The numbers of $ad$ in $[0]$ are $2p-1$ and in other class is $p-1$.

It's easy to get the sum is $p(p+1)(p-1)^2.$

### (b)

You may check case by case or note that the numbers which given $ad-bc = k$ are the same for $k > 0.$

Hence $o(H) = o(G)/(p-1) = p(p+1)(p-1)$

I prefer the second way. 😋