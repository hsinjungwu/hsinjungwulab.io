---
title: "Topics in Algebra"
cascade:
  tags: ["Algebra"]
---

Topics in Algebra (2nd Edition) by [I. N. Herstein](https://en.wikipedia.org/wiki/Israel_Nathan_Herstein)